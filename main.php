<?php

require_once("./LightPHP/lp-load.php");
require_once("config.php");
require_once("handler/global.php");

lpMVC::bind('^/(\d+)?/?$',function()
{
    require_once("handler/view.php");
    return new JBIndexView;
});

lpMVC::bind('^/topic/(\d+)/?',function()
{
    require_once("handler/topic.php");
    return new JBTopic;
});

lpMVC::bind('^/new/?',function()
{
    require_once("handler/topic.php");
    return new JBNewTopic;
});

lpMVC::bind('^/login/?',function()
{
    require_once("handler/user.php");
    return new JBLogin;
});

lpMVC::bind('^/logout/?',function()
{
    require_once("handler/user.php");
    return new JBLogout;
});

lpMVC::bind('^/signup/?',function()
{
    require_once("handler/user.php");
    return new JBSignup;
});

lpMVC::onDefault(function()
{
    return "404 - 不存在对应的处理器";
});

?>

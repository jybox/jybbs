<?php if(!isset($lpInTemplate)) die();

$tmp=new lpTemplate;

$a["title"]=$topic["title"];

lpBeginBlock();?>



<?php
$a["sidebar"]=lpEndBlock();

?>
<div class="box well">
    <header>
      <h1><?= $topic["title"];?></h1>
    </header>
    <article>
      <?= $topicContent;?>
    </article>
    <hr class="small" />
    <footer>
    	创建于 <span title="<?= gmdate("Y.m.d H:i:s",$topic["topictime"]);?>"><?= lpTools::niceTime($topic["topictime"]);?></span>
    </footer>
</div>
<div class="box well">
    <header>回复(<?= $topic["comments"];?>)</header>
    <? foreach($comments as $comment): ?>
      <div class="list">
        <img class="avatar" src="<?= $comment["avatar"];?>" />
        <p>
          <a href="#"><?= $comment["uname"];?></a> | <?= $comment["floor"];?>#
        </p>
        <div class="reply-content">
        	<?= $comment["body"];?>
        	<hr class="small" />
        	<footer>
		        创建于 <span title="<?= gmdate("Y.m.d H:i:s",$comment["time"]);?>"><?= lpTools::niceTime($comment["time"]);?></span>
		      </footer>
        </div>
      </div>
    <? endforeach; ?>
</div>
<div class="box well">
    <header>创建回复</header>
    <form class="form-horizontal" id="form" method="post">
		    <div id="errorTips" class="alert alert-error<?= isset($errorMsg)?"":" hide";?>">
		      <header>错误</header> <span id="errorBody"><?= isset($errorMsg)?$errorMsg:"";?></span>
		    </div>
        <textarea id="content" name="content" rows="10" required="required"><?= isset($content)?$content:"";?></textarea>
        <button type="submit" class="btn btn-primary">提交</button>
    </form>
</div>

<?php

$tmp->parse("template/base.php",$a);

?>

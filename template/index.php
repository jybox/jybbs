<?php if(!isset($lpInTemplate)) die();

$tmp=new lpTemplate;

$a["title"]="主页";

lpBeginBlock();?>

$prePage=$page-1;
$nextPage=$page+1;

<?php
$a["sidebar"]=lpEndBlock();

?>

<div class="box well">
	<? foreach($topics as $topic): ?>
	  <div class="list">
	      <img class="avatar" src="<?= $topic["avatar"];?>" />
	      <p>
	          <a href="/topic/<?= $topic["id"];?>/"><?= $topic["title"];?></a>
	
	          <a href="/topic/<?= $topic["id"];?>/" class="badge pull-right"><?= $topic["comments"];?></a>
	      </p>
	      <p>
	          <a href="#"><?= $topic["uname"];?></a>创建于<span title="<?= gmdate("Y.m.d H:i:s",$topic["topictime"]);?>"><?= lpTools::niceTime($topic["topictime"]);?></span>，
	          <a href="#"><?= $topic["lastcommentuser"];?></a>最后回复于<span title="<?= gmdate("Y.m.d H:i:s",$topic["lastcommenttime"]);?>"><?= lpTools::niceTime($topic["lastcommenttime"]);?></span>
	      </p>
	  </div>
	<? endforeach; ?>
	<footer>
		<ul class="pager">
			<? if($prePage): ?>
			  <li class="previous">
			    <a href="/<?= $prePage;?>/"> &lt;&lt; 上一页</a>
			  </li>
		  <? endif; ?>
		  <? if($nextPage): ?>
			  <li class="next">
			    <a href="/<?= $nextPage;?>/"> &gt;&gt; 下一页</a>
			  </li>
		  <? endif; ?>
		</ul>
	</footer>
</div>


<?php

$tmp->parse("template/base.php",$a);

?>

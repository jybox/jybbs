<?php if(!isset($lpInTemplate)) die();

if(!isset($topicTitle))
{
    $topicTitle="";
    $a["title"]=isset($title)?$title:"编辑新帖子";
}
else
{
		$a["title"]="编辑 " . $topicTitle;
}

$tmp=new lpTemplate;

lpBeginBlock();?>



<?php
$a["sidebar"]=lpEndBlock();

?>
<div class="box well">
  <header><?= $a["title"];?></header>
  <form class="form-horizontal" id="form" method="post">
    <div id="errorTips" class="alert alert-error<?= isset($errorMsg)?"":" hide";?>">
      <header>错误</header> <span id="errorBody"><?= isset($errorMsg)?$errorMsg:"";?></span>
    </div>
    <fieldset>
      <input type="text" class="input-xxlarge" id="title" name="title" placeholder="标题" maxlength="100" value="<?= $topicTitle;?>" required="required" />
      <textarea id="content" name="content" rows="20"><?= isset($topicContent)?$topicContent:"";?></textarea>
      <div class="form-actions submitLeft">
          <button type="submit" class="btn btn-primary btn-large">提交</button>
      </div>
    </fieldset>
  </form>
</div>

<?php

$tmp->parse("template/base.php",$a);

?>
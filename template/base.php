<?php if(!isset($lpInTemplate)) die();

global $JbBBSName,$JbBBSInfo;

$title .= " | {$JbBBSName}";

?>
<!DOCTYPE html>
<html lang="zh">
  <head>
    <meta charset="utf-8">
    <title><?= $title;?></title>
    <?= lpTools::linkTo("bootstrap",NULL,false); ?>
    <?= lpTools::linkTo("bootstrap-responsive",NULL,false); ?>
    <?= lpTools::linkTo("lp-css"); ?>
    <link href="/static/style/global.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="/"><?= $JbBBSName;?></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="/">主页</a></li>
            </ul>
            <ul class="nav pull-right">
            <? if(lpAuth::login()): ?>
              <li><a href="/new/">创建主题</a></li>
              <li></li>
              <li><a href="/profile/"><?= lpAuth::getUName();?></a></li>
              <li><a href="/logout/">注销</a></li>
            <? else: ?>
              <li><a href="/signup/">注册</a></li>
              <li><a href="/login/">登录</a></li>
            <? endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="main" class="container">
      <div class="row">
        <div class="span9">
          <?= $lpContents;?>
        </div>
          <div class="span3">
          	<? if(isset($sidebarc)): ?>
            	<?= $sidebar["header"];?>
            <? endif; ?>
            <div class="box well">
              <header>公告</header>
              <?= $JbBBSInfo;?>
            </div>
            <div class="box well">
                <ul>
                  <li><script src="https://hm.baidu.com/h.js?77016691cd5a049005dba568b5164b59" type="text/javascript"></script></li>
                  <li>JyBBS托管于<a href="https://github.com/jybox/JyBBS">Github</a></li>
                </ul>
            </div>
            <? if(isset($sidebar["footer"])): ?>
            	<?= $sidebar["footer"];?>
            <? endif; ?>
          </div>
      </div>
      <? if(isset($footer)): ?>
        <hr />
        <footer>
          <?= $footer;?>
        </footer>
      <? endif; ?>
    </div>
    <?= lpTools::linkTo("jquery",NULL,false); ?>
    <?= lpTools::linkTo("bootstrap-js",NULL,false); ?>
    <?= isset($endOfBody)?$endOfBody:"";?>
  </body>
</html>


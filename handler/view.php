<?php

class JBIndexView extends lpPage
{
    public function get($args)
    {
    	  global $jbCfgTopicPerPage;
    	
        if(!isset($args[0]))
            $args[0]=1;

        $conn=new lpMySQL;

        $rs=$conn->select("topic",array(),"lastcommenttime",($args[0]-1) * $jbCfgTopicPerPage , $jbCfgTopicPerPage,false);

        $rs=$rs->toArray();

        foreach($rs as $key=>$value)
        {
            $rsU=$conn->select("user",array("uname"=>$rs[$key]["uname"]));
            $rsU->read();
            $rs[$key]["avatar"]=lpTools::gravatarUrl($rsU->email);
        }
        
        
        if($args[0]>1)
        	$prePage=$args[0]-1;
        else
        	$prePage=0;
        	
        $allTopic=$conn->select("topic");
        	
        if($allTopic->num() > $args[0] * $jbCfgTopicPerPage)
        	$nextPage=$args[0]+1;
        else
        	$nextPage=0;

        $tmp=new lpTemplate;

        $tmp->parse("template/index.php",array("topics"=>$rs,"prePage"=>$prePage,"nextPage"=>$nextPage));
        
        return true;
    }
}

?>

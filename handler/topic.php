<?php

class JBNewTopic extends lpPage
{
    private $msg;
        
    public function get()
    {
        if(!lpAuth::login())
        {
            $this->gotoURL("/login/?next=" . urldecode($_SERVER["REQUEST_URI"]));
            $this->httpCode=302;
            return true;
        }
        
        $a["topicTitle"]="编辑新帖子";
        
        return lpTemplate::parseFile("template/editTopic.php",$a);
    }

    public function post()
    {
        global $lpCfgTimeToChina;
            
        if(!lpAuth::login())
        {
            lpBeginBlock();?>
              <a href="<?= "/login/?next=" . urldecode($_SERVER["REQUEST_URI"]);?>">请点击这里登录</a>
            <?php
            $this->msg=lpEndBlock();
            
            return false;
        }

        if(!isset($_POST["title"]) or strlen($_POST["title"])>100 or $_POST["title"]=="")
        {
            $this->msg="标题应不大于100个字符，且不能为空";
            return false;
        }

        $content=isset($_POST["content"])?$_POST["content"]:"";

        $conn=new lpMySQL;

        $row["uname"]=lpAuth::getUName();
        $row["title"]=$_POST["title"];
        $row["comments"]=0;
        $row["lastcommentuser"]=lpAuth::getUName();
        $row["lastcommenttime"]=time()+$lpCfgTimeToChina;
        $row["topictime"]=time()+$lpCfgTimeToChina;

        $conn->insert("topic",$row);

        $row=array();
        $row["topicid"]=$conn->insertId();
        $row["uname"]=lpAuth::getUName();
        $row["body"]=nl2br(str_ireplace(" ","&nbsp;",htmlspecialchars($content)));
        $row["ua"]=$_SERVER["HTTP_USER_AGENT"];
        $row["time"]=time()+$lpCfgTimeToChina;
        $row["ip"]=lpTools::getIP();
        $row["floor"]=0;

        $conn->insert("comment",$row);
        
        $this->gotoUrl("/topic/{$row["topicid"]}/");
        return true;
    }
    
    public function procError()
    {
        $this->httpCode=400;
        
        $tmp=new lpTemplate;
        
        $a["errorMsg"]=$this->msg;
        $a["title"]=$_POST["title"];
        $a["topicContent"]=$_POST["content"];
            
        $tmp->parse("template/editTopic.php",$a);
    }
}

class JBTopic extends lpPage
{
    private $msg;
        
    public function get($args)
    {
        if(!isset($args[0]))
        {
            $this->httpCode=404;
            return false;
        }

        $tid=lpTools::rxMatch('/\d+/',$args[0]);
        if(!$tid)
        {
            $this->httpCode=404;
            return false;
        }

        $conn=new lpMySQL;
        $rs=$conn->select("topic",array("id"=>$tid));

        if($rs->read())
        {
            $topicInfo=$rs->rawArray();

            $rsCmd=$conn->select("comment",array("topicid"=>$tid));
            $rsCmd->read();
            $topicContent=$rsCmd->body;

            $comments=$rsCmd->toArray();


            foreach($comments as $key=>$value)
            {
                $rsU=$conn->select("user",array("uname"=>$comments[$key]["uname"]));
                $rsU->read();
                $comments[$key]["avatar"]=lpTools::gravatarUrl($rsU->email);
            }

        }
        else
        {
            $this->httpCode=404;
            return false;
        }

        $tmp=new lpTemplate;

        $tmp->parse("template/topic.php",array("comments" => $comments,
                                               "tid" => $tid,
                                               "topic" => $topicInfo,
                                               "topicContent" => $topicContent));
        return true;
    }

    public function post($args)
    {
        global $lpCfgTimeToChina;
            
        if(!isset($args[0]))
        {
            $this->msg="主题不存在";
            return false;
        }

        $tid=lpTools::rxMatch('/\d+/',$args[0]);
        if(!$tid)
        {
            $this->msg="主题不存在";
            return false;
        }

        if(!lpAuth::login())
        {
            lpBeginBlock();?>

            <a href="<?= "/login/?next=" . urldecode($_SERVER["REQUEST_URI"]);?>">请点击这里登录</a>

            <?php
            $this->msg=lpEndBlock();
            
            return false;
        }

        if(!isset($_POST["content"]))
        {
            $this->msg="请填写内容";
            return false;
        }

        $conn=new lpMySQL;

        $rs=$conn->select("topic",array("id"=>$tid));

        if($rs->read())
        {
            $row["comments"]=$rs->comments+1;
            $row["lastcommentuser"]=lpAuth::getUName();
            $row["lastcommenttime"]=time()+$lpCfgTimeToChina;

            $conn->update("topic",array("id"=>$tid),$row);

            $row=array();
            $row["topicid"]=$tid;
            $row["uname"]=lpAuth::getUName();
            $row["body"]=nl2br(str_ireplace(" ","&nbsp;",htmlspecialchars($_POST["content"])));
            $row["ua"]=$_SERVER["HTTP_USER_AGENT"];
            $row["time"]=time()+$lpCfgTimeToChina;
            $row["ip"]=lpTools::getIP();
            $row["floor"]=$rs->comments+1;

            $conn->insert("comment",$row);

            $this->gotoUrl($_SERVER["REQUEST_URI"]);
            return true;
        }
        else
        {
            $this->msg="主题不存在";
            return false;
        }
    }
    
    public function procError()
    {
        if($this->httpCode==404)
        {
            echo "404 - 主题不存在";
            return;
        }
        
        if(strtolower($_SERVER["REQUEST_METHOD"])=="post")
        {
            $this->httpCode=400;
            
            $tmp=new lpTemplate;
            
            $a["errorMsg"]=$this->msg;
            $a["content"]=$_POST["content"];
                
            $tmp->parse("template/editTopic.php",$a);
        }
    }
}

?>
